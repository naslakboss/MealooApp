package codebuddies.MealooApp.entities.user;

public enum MealooUserRole {
    USER,
    MODERATOR,
    ADMIN
}
