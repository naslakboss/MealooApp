package codebuddies.MealooApp.entities.user;

public enum WeightGoal {
    LOSTHALFKGPERWEEK,
    LOSTQUARTERKGPERWEEK,
    MAINTAIN,
    GAINQUARTERKGPERWEEK,
    GAINHALFKGPERWEEK
}
